let punchedCards = document.getElementById('punchedCards')
let floppy = document.getElementById('floppy')
let cd = document.getElementById('cd')
let sd = document.getElementById('sd')
let cloud = document.getElementById('cloud')
let discount = document.getElementById('discount')
// let points = document.getElementById('points')

let blocks = []
let userPoints = 0
blocks.push(punchedCards, floppy, cd, sd, cloud, discount, points)

blocks.forEach(el => {
    el.classList.add('hide')
})

document.getElementById('start').addEventListener('click', el => {
    console.log('start')
    main.classList.add('hide')
    punchedCards.classList.remove('hide')
    points.classList.remove('hide')
    document.getElementById('displayPoints').innerHTML = 0
})
// 
document.getElementsByClassName('answer_1')[0].addEventListener('click', el => {
    punchedCards.classList.add('hide')
    floppy.classList.remove('hide')
    updatePoints(2)
})
document.getElementsByClassName('answer_2')[0].addEventListener('click', el => {
    punchedCards.classList.add('hide')
    floppy.classList.remove('hide')
})
document.getElementsByClassName('answer_3')[0].addEventListener('click', el => {
    punchedCards.classList.add('hide')
    floppy.classList.remove('hide')
})
//
document.getElementsByClassName('answer_1')[1].addEventListener('click', el => {
    floppy.classList.add('hide')
    cd.classList.remove('hide')
})
document.getElementsByClassName('answer_2')[1].addEventListener('click', el => {
    floppy.classList.add('hide')
    cd.classList.remove('hide')
})
document.getElementsByClassName('answer_3')[1].addEventListener('click', el => {
    floppy.classList.add('hide')
    cd.classList.remove('hide')
    updatePoints(2)
})
//
document.getElementsByClassName('answer_2')[2].addEventListener('click', el => {
    cd.classList.add('hide')
    sd.classList.remove('hide')
    updatePoints(2)
})
document.getElementsByClassName('answer_1')[2].addEventListener('click', el => {
    cd.classList.add('hide')
    sd.classList.remove('hide')
})
document.getElementsByClassName('answer_3')[2].addEventListener('click', el => {
    cd.classList.add('hide')
    sd.classList.remove('hide')
})
//
document.getElementsByClassName('answer_3')[3].addEventListener('click', el => {
    sd.classList.add('hide')
    cloud.classList.remove('hide')    
    updatePoints(2)
})
document.getElementsByClassName('answer_1')[3].addEventListener('click', el => {
    sd.classList.add('hide')
    cloud.classList.remove('hide')    
})
document.getElementsByClassName('answer_2')[3].addEventListener('click', el => {
    sd.classList.add('hide')
    cloud.classList.remove('hide')    
})
//
document.getElementsByClassName('answer_2')[4].addEventListener('click', el => {
    cloud.classList.add('hide')
    discount.classList.remove('hide')
    updatePoints(2)
    document.querySelector('#animation-bits svg').style.display = 'none'
})
document.getElementsByClassName('answer_1')[4].addEventListener('click', el => {
    cloud.classList.add('hide')
    discount.classList.remove('hide')
    document.querySelector('#animation-bits svg').style.display = 'none'
})
document.getElementsByClassName('answer_3')[4].addEventListener('click', el => {
    cloud.classList.add('hide')
    discount.classList.remove('hide')
    document.querySelector('#animation-bits svg').style.display = 'none'
})
//
document.getElementById('restart').addEventListener('click', el => {
    discount.classList.add('hide')
    main.classList.remove('hide')
    updatePoints(-1)
    points.classList.add('hide')
    bitsAnimation.play()
    document.querySelector('#animation-bits svg').style.display = 'inline-block'
})

function updatePoints(points) {
    if (points > 0) {
        userPoints += points
        document.getElementById('displayPoints').innerHTML = userPoints
    } else {
        userPoints = 0
        document.getElementById('displayPoints').innerHTML = userPoints
    }
}

document.getElementById('animation-bits').addEventListener('mouseover', () => [
    bitsAnimation.play()
])
document.getElementById('animation-bits').addEventListener('mouseout', () => [
    setTimeout(() => {
        bitsAnimation.stop()
    }, 6000)
])

var bitsAnimation = bodymovin.loadAnimation({
    container: document.getElementById('animation-bits'),
    renderer: 'svg',
    loop: true,
    autoplay: false,
    path: './animations/bits/bits.json'
  })
var cdAnimation = bodymovin.loadAnimation({
    container: document.getElementById('animation-cd'),
    renderer: 'svg',
    loop: true,
    autoplay: false,
    path: './animations/cd/cd.json'
  })
var cloudAnimation = bodymovin.loadAnimation({
    container: document.getElementById('animation-cloud'),
    renderer: 'svg',
    loop: true,
    autoplay: false,
    path: './animations/cloud/cloud.json'
  })
var flashAnimation = bodymovin.loadAnimation({
    container: document.getElementById('animation-flash'),
    renderer: 'svg',
    loop: true,
    autoplay: false,
    path: './animations/flash/flash.json'
  })
var floppyAnimation = bodymovin.loadAnimation({
    container: document.getElementById('animation-floppy'),
    renderer: 'svg',
    loop: true,
    autoplay: false,
    path: './animations/floppy/floppy.json'
  })
var hardAnimation = bodymovin.loadAnimation({
    container: document.getElementById('animation-hard'),
    renderer: 'svg',
    loop: true,
    autoplay: false,
    path: './animations/hard/hard.json'
  })
var punchedAnimation = bodymovin.loadAnimation({
    container: document.getElementById('animation-punched'),
    renderer: 'svg',
    loop: true,
    autoplay: false,
    path: './animations/punched/punched.json'
  })
var sdAnimation = bodymovin.loadAnimation({
    container: document.getElementById('animation-sd'),
    renderer: 'svg',
    loop: true,
    autoplay: false,
    path: './animations/sd/sd.json'
  })
var wifiAnimation = bodymovin.loadAnimation({
    container: document.getElementById('animation-wifi'),
    renderer: 'svg',
    loop: true,
    autoplay: false,
    path: './animations/wifi/wifi.json'
  })
var finAnimation = bodymovin.loadAnimation({
    container: document.getElementById('animation-fin'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './animations/fin/fin2.json'
  })
  let allAnimations = [cdAnimation, cloudAnimation, flashAnimation, floppyAnimation, hardAnimation, punchedAnimation, sdAnimation, wifiAnimation]
  cdAnimation.goToAndStop(100, true)
  cloudAnimation.goToAndStop(100, true)
  flashAnimation.goToAndStop(150, true)
  floppyAnimation.goToAndStop(100, true)
  hardAnimation.goToAndStop(200, true)
  punchedAnimation.goToAndStop(200, true)
  sdAnimation.goToAndStop(200, true)
  wifiAnimation.goToAndStop(200, true)
console.log(wifiAnimation)

  document.getElementById('start').addEventListener('click', () => {
    allAnimations.forEach(el => {
        el.play()
        el.setSpeed(3)
    })
    document.getElementById('animation-cd').style.animationName = ''
    document.getElementById('animation-cloud').style.animationName = ''
    document.getElementById('animation-flash').style.animationName = ''
    document.getElementById('animation-floppy').style.animationName = ''
    document.getElementById('animation-hard').style.animationName = ''
    document.getElementById('animation-punched').style.animationName = ''
    document.getElementById('animation-sd').style.animationName = ''
    document.getElementById('animation-wifi').style.animationName = ''
  })
